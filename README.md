DISPLAY INFORMATIVO CON MODULOS DE MATRICES DE LED


El proyecto consiste en diseñar un display ccontrolado por una raspberry pi 3 que cumpla la funcion de informar
acerca de la hora, la temperatura ambiente local (mediante un sensor) y mundial (mediante un acceso a internet).
A su vez, manda mensajes recordatorios ya pregrabados en el programa.
Mediante codigo assembler, indicar con tres leds monocromaticos de distintos colores (o un RGB), si la temperatura local es baja, media o alta.

FUNCIONES OPCIONALES A EVALUAR:
*Mostrar noticias de paginas web
*Mostrar tweets respecto a un hashtag
*Configurar el sistema mediante una aplicacion movil por bluetooth

nos basanos en https://hackaday.io/project/15832-weather-ticker